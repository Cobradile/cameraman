extends Spatial

export(NodePath) var heid
var heidThrawn = false

# Called when the node enters the scene tree for the first time.
func _ready():
	if !heid.is_empty():
		heid = get_node(heid)

func _input(event):
	if Input.is_action_just_pressed("shuit"):
		if !heidThrawn:
			flingHeid()

func flingHeid():
	heidThrawn = true
	$"../Head/body".show()
	$Camera.current = false
	heid.transform = $Camera/Position3D.global_transform
	heid.player = $".."
#	heid.add_central_force(Vector3(0, 0, -100))
#	heid.sleeping = false
	heid.get_node("Camera").current = true
	heid.fling($Camera/Position3D.global_transform.origin, $Camera/Position3D2.global_transform.origin)

func heid_back():
	$"../Head/body".hide()
	$Camera.current = true
	heidThrawn = false

func _on_Heid_body_entered(body):
	print("BANG!")
