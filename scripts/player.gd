extends KinematicBody

export(bool) var flyMode = false setget setFlyMode

export(float, 0, 3) var lookTurnIntensity = 1

export(int) var maximumHealth = 200
export(int) var defaultHealth = 100
onready var health = defaultHealth

export(int) var maximumArmour = 200
export(int) var defaultArmour = 100
onready var armour = 0

export(int) var maximumSprint = 200
export(int) var defaultSprint = 100
onready var sprint = defaultSprint

const MAX_EE_PROTECTOR_HALTH = 100
var eeProtectorHalth = 0 setget setEeProtectorHealth
const MAX_EE_HALTH = 150
onready var eeHalth = float(MAX_EE_HALTH) setget setEeHalth
var kill_timer = 0

var currentState = 0 setget setState, getState # The current state o the player.
# -3 = inactive
# -2 = Level end
# -1 = Deed
# 0 = staundin
# 1 = muivin
# 2 = croochin
# 2.5 = crooch-muivin
# 3 = jimpin
# 4 = sprintin
# 5 = catchin breath
# 6 = aimin
# 7 = fleein
# 8 = chynginWapens
# 9 = jimp sprentin
# 10 = cheat
onready var previousState = currentState
var defaultState = 0

var camera_angle = 0
#onready var _mouse_sensitivity = float(config.get_value("Config", "MouseSensitivity")) / 100
var _mouse_sensitivity = 1
onready var mouse_sensitivity = _mouse_sensitivity
var camera_change = Vector2()

var velocity = Vector3()
var previousVelocity = Vector3()
var direction = Vector3()
var direzione = 0

# fly variables
const FLY_SPEED = 20
const FLY_ACCEL = 4

#walk variables
var gravity = -9.8 * 3
const MAX_SPEED = 16
const MAX_RUNNING_SPEED = 30
const MAX_REKIVERIN_SPEED = 10
const MAX_ADS_SPEED = 1
const EXPLODE_SPEED = 20
const ACCEL = 10
const DEACCEL = 7

#jumping
var jump_height = 10
var has_contact = false

#slipe variables
const MAX_SLOPE_ANGLE = 100

#stair variables
const MAX_STAIR_SLOPE = 20
const STAIR_JUMP_HEIGHT = 6

onready var currentAnim setget setAnim

var lastShot
var look_direction = Vector3()

export(String) var body = "Female"

var haesBody = true
var haesSFX = false
var isInLevel = false
onready var shuitRay = $"Head/Camera/FPS Arms/shuitRay"

onready var cameraY = $Head/Camera.translation.y
const EXPLOSION_COOLER = 100
var explosion_timer = 0 setget set_explosion_timer
onready var timer_slowdown = explosion_timer
var explosion_severity = 0.1

const LAND_COOLER = 3
var land_timer = 0

#export(String) var staundinAnimation = "Idle"
#export(PoolStringArray) var walkinAnimations # 0 = up, 1 = doun, 2 = left an 3 = richt
var currentDirection = 0 # This will determine what direction the model will be going for the animation.
#export(String) var croochAnimation = "FPSCrooch"
#export(String) var jimpAnimation = "FPSJimpin"
#export(String) var sprentAnimation = "Rin"

var yaw = 0
var pitch = 0
onready var prevPitch = pitch
onready var prevYaw = yaw
const TURN_COOLER = 0.01
onready var turnTimer = TURN_COOLER
const LOOK_COOLER = 0.005
onready var lookTimer = LOOK_COOLER

var rekiveryTime = 10
var previousSpeed

export(Array) var inventory

var hitSource

signal setSprint
signal unlockWapen
signal chyngeSprent
signal deid
signal turnArms
signal setActive
signal returnResult
signal shootProjectile

signal somethingDetected
signal gatKeycaird

signal playLocalSFX
signal applySFXRange

signal shawHalth
signal addArmour
signal addAmmo
signal showHideHUDMouse
signal getObjectives

signal playSFX

signal toggleGoggles

signal showObject

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	if !has_node("Head/FPSBody"):
		haesBody = false
	if has_node("SoondFX"):
		print(connect("playSFX", $"SoondFX", "play"))
	
	if has_node("Head/Camera/FPS Arms"):
		print(connect("setSprint", $"Head/Camera/FPS Arms", "sprint"))
		print(connect("unlockWapen", $"Head/Camera/FPS Arms", "unlockWeapon"))
		print($"Head/Camera/FPS Arms".connect("ADSMode", self, "ADS_Mode"))
		print($"Head/Camera/FPS Arms".connect("resetAim", self, "resetAim"))
		print(connect("returnResult", $"Head/Camera/FPS Arms", "lock_on"))
		print(connect("shootProjectile", $"Head/Camera/FPS Arms", "set_projectile"))
		print(connect("turnArms", $"Head/Camera/FPS Arms", "lookTurn"))
		print(connect("setActive", $"Head/Camera/FPS Arms", "set_process"))
		print(connect("addAmmo", $"Head/Camera/FPS Arms", "addAmmo"))
		print(connect("deid", $"Head/Camera/FPS Arms", "queue_free"))
		if has_node("HUD"):
			print($"Head/Camera/FPS Arms".connect("updateAmmo", $HUD, "setAmmo"))
			print($"Head/Camera/FPS Arms".connect("ADSRandom", $HUD, "setRandomSpreid"))
			print($"Head/Camera/FPS Arms".connect("updateIcons", $HUD, "setIcons"))
			print($"Head/Camera/FPS Arms".connect("pauseGemm", $HUD, "pauseMode"))
			print($"Head/Camera/FPS Arms".connect("reloadAlert", $HUD, "show_reload"))
		$"Head/Camera/FPS Arms".body = self
	
#	print(cheatcodes.connect("naeClip", self, "naeClip"))
	
	if has_node("SoondFX"):
		print($"Head/Camera/FPS Arms".connect("playLocalSFX", $SoondFX, "play"))
	
	if get_tree().get_root().has_node("Level"):
		print(connect("deid", get_tree().get_root().get_node("Level"), "dead"))
	
	if has_node("HUD"):
		print(connect("shawHalth", $HUD, "setHealth"))
		print(connect("showHideHUDMouse", $HUD, "setHUDCrosshair"))
		print($HUD.connect("ADSAim", self, "ADSAim"))
		print($HUD.connect("playSFX", $SoondFX, "play"))
		print(connect("toggleGoggles", $HUD, "toggleGoggles"))
		print(connect("setActive", $HUD, "set_active"))
		$HUD/HalthBaur.value = defaultHealth
#		connect("chyngeSprent", $HUD, "setSprint")
#		connect("somethingDetected", $HUD, "on_something_detect")
#		connect("gatKeycaird", $HUD, "gat_keycaird")
#		connect("deid", $HUD, "death")
#		connect("shawHalth", $HUD, "showHealthArmour")
#		connect("addAmmo", $HUD, "addAmmo")
		connect("addArmour", $HUD, "addArmour")
	
	if has_node("SoondFX"):
		print(connect("playLocalSFX", $SoondFX, "play"))
	
	previousSpeed = MAX_SPEED
	
	if has_node("DeathMusic"):
		var vollum = 10
		if vollum <= 0:
			$DeathMusic.queue_free()
		else:
			$DeathMusic.volume_db = linear2db(pow(float(vollum)/100 , 2))

func _process(delta):
	if currentState >= 0:
		if Engine.get_frames_drawn() % 60 == 0:
			if defaultHealth > maximumHealth:
				defaultHealth = maximumHealth
#		elif Engine.get_frames_drawn() % 20 == 0:
#			#This chacks gin the shuitRay is titchin onything that is o interest, an chynge the leuk o the crosshair accordin-like.
#			if shuitRay.is_colliding() and is_instance_valid(shuitRay.get_collider()):
#				if shuitRay.get_collider().is_in_group("Pick-Up"):
#					emit_signal("somethingDetected", "Eetem")
#				elif shuitRay.get_collider().is_in_group("Enemy"):
#					emit_signal("somethingDetected", "Baddie")
#				elif shuitRay.get_collider().is_in_group("Victim"):
#					emit_signal("somethingDetected", "Victim")
#				else:
#					emit_signal("somethingDetected", "Idle")
#			else:
#				emit_signal("somethingDetected", "Idle")
		
		if turnTimer > 0:
			turnTimer -= delta
		if lookTimer > 0:
			lookTimer -= delta
		
		#Gin the player is sprentin (state 4), deplete the sprent amoont 'till hit gits tae 0.
		#Gin it reaches there, chynge the state tae catchin breath (5), whaur ye canna rin, jump or crooch.
		#Gin the player lats gae o the sprent command afore that, increase the sprent back, an function like ordinar.
		if currentState == 4:
			sprint -= delta * 50
			if sprint <= 0:
				self.currentState = 5
				emit_signal("setSprint", false)
			emit_signal("chyngeSprent", sprint)
		elif currentState == 5:
			sprint += delta * rekiveryTime
			if sprint >= defaultSprint:
				self.currentState = 1
			emit_signal("chyngeSprent", sprint)
		elif currentState != 3:
			if sprint < defaultSprint:
				sprint += delta * 30
				emit_signal("chyngeSprent", sprint)
	
	if explosion_timer > 0:
		if !is_instance_valid($Head/Camera):
			return
		$Head/Camera.translation.y = cameraY + (sin(explosion_timer) * explosion_severity)
		explosion_timer -= delta * timer_slowdown
		if timer_slowdown > 1:
			timer_slowdown -= delta * 50
		else:
			self.explosion_timer = 0
			$Head/Camera.translation.y = cameraY
	elif land_timer > 0:
		land_timer -= delta * 5
		$Head/Camera.translation.y = cameraY + sin(land_timer) * (previousVelocity / 15)
	
#	if eeHalth < float(MAX_EE_HALTH) and eeHalth > 0:
#		self.eeHalth += (20 - (graphics.difficulty * 2)) * delta
#	elif eeHalth == 0:
#		eeHalth = -1
#		$"Kill Timer".start(0.5)
	
	aim() # This caas the function that taks care o the swayin an muivement on the FPS airms while muivin
	
	#if hitSource:
		#setBack(hitSource, delta)

func set_explosion_timer(value):
	explosion_timer = value
	timer_slowdown = value

func setBack(hitSource, delta):
	velocity -= move_and_slide((hitSource - global_transform.origin).normalized() * delta * velocity)

func unlockWapen(newWapen, ammo):
	if ammo < 0:
		ammo = 0
	self.currentState = 8
	emit_signal("unlockWapen", newWapen, ammo)

func _physics_process(delta):
	if currentState > -1:
		if currentState == 7 or flyMode:
			fly(delta)
		else:
			walk(delta)

func toggle_cheat(value):
	if value:
		self.currentState = 10

func _input(event):
	if Input.is_action_just_pressed("deactivate"):
		if currentState > -3:
			#self.currentState = -3
			pass
		else:
			self.currentState = 0
	
	if has_node("HUD"):
		if Input.is_action_pressed("hud_status"):
			if !$HUD/Status2.visible:
				emit_signal("getObjectives")
				$HUD/Status2.show()
				$HUD/AnimationPlayer.play("Shaw Halth")
	#		$HUD/ArmourBaur.show()
	#		$HUD/ArmourBaur.modulate = Color.white
	#		$HUD/HalthBaur.show()
	#		$HUD/HalthBaur.modulate = Color.white
		elif $HUD/Status2.visible:
			$HUD/Status2.hide()
	#		$HUD/ArmourBaur.hide()
	#		$HUD/HalthBaur.hide()
	
	if Input.is_action_just_pressed("Test"):
		emit_signal("showObject", $"Head/Camera/FPS Arms/shuitRay/Crosshair".global_transform.origin, "Key")
	
	if currentState > -1 and canAim():
		if event is InputEventMouseMotion: # Gin the moose is muivin
			if lookTimer <= 0:
				camera_change = event.relative
				yaw = fmod(yaw - event.relative.x * mouse_sensitivity, 360)
				pitch = max(min(pitch - event.relative.y * mouse_sensitivity, 90), -90)
				lookTimer = LOOK_COOLER
		elif event is InputEventKey:
			if Input.is_action_just_pressed("use"):
				if $Head/Camera/meleeRay.is_colliding():
					var collider = $Head/Camera/meleeRay.get_collider()
					if $Head/Camera/meleeRay.get_collider().is_in_group("Door"):
						$Head/Camera/meleeRay.get_collider().tryDoor(self)
					elif $Head/Camera/meleeRay.get_collider().is_in_group("Interactive"):
						$Head/Camera/meleeRay.get_collider().interact()
						if $Head/Camera/meleeRay.get_collider().is_in_group("PC"):
							emit_signal("playSFX", "Keyboard")

func walk(delta):
	# reset the direction o the player
	direction = Vector3()
	direzione = -1
	
	# git the rotation o the camers
	var aim = $Head/Camera.get_global_transform().basis
	
	# chack inpit an chynge direction
	if Input.is_action_pressed("move_forward"):
		direzione = 0
		direction -= aim.z
		chyngeAnim()
	elif Input.is_action_pressed("move_backward"):
		if currentState != 4:
			direzione = 1
			direction += aim.z
			chyngeAnim()
	
	if Input.is_action_pressed("move_left"):
		if currentState != 4:
			direzione = 2
			direction -= aim.x
			chyngeAnim()
	elif Input.is_action_pressed("move_right"):
		if currentState != 4:
			direzione = 3
			direction += aim.x
			chyngeAnim()
	
	if direzione < 0:
		chyngeAnim()
	direction.y = 0
	
	direction = direction.normalized()
	
	if is_on_floor():
		if currentState == 3:
			land_timer = LAND_COOLER
			self.currentState = defaultState
		if currentState != 4 and currentState != 6:
			if sprint < defaultSprint:
				self.currentState = 5
		if currentState != 5 and currentState != 4 and currentState != 6:
			if direction.length() > 0:
				if Input.is_action_pressed("crooch") or $Heid.is_colliding():
					land_timer = 0
					self.currentState = 2.5
				elif !$Heid.is_colliding():
					self.currentState = 1
			else:
				if Input.is_action_pressed("crooch") or $Heid.is_colliding():
					land_timer = 0
					self.currentState = 2
				elif !$Heid.is_colliding():
					self.currentState = 0
#		else:
#			if direction.length() > 0:
#				if haesBody:
#					self.currentAnim = walkinAnimations[direzione]
#				rekiveryTime = 10
#			else:
#				self.currentAnim = staundinAnimation
#				rekiveryTime = 30
		has_contact = true
		
		if is_on_wall():
			var n = $Tail.get_collision_normal()
			var floor_angle = rad2deg(acos(n.dot(Vector3(0, 1, 0))))
			if floor_angle > MAX_SLOPE_ANGLE:
				velocity.y += gravity * delta
	else:
		if !$Tail.is_colliding():
			self.currentState = 3
			has_contact = false
			previousVelocity = velocity.y
		velocity.y += gravity * delta
	
	if has_contact and !is_on_floor():
		move_and_collide(Vector3(0, -1, 0))
	
	
	var temp_velocity = velocity
	temp_velocity.y = 0
	
	var speed = previousSpeed
	if Input.is_action_pressed("move_sprint") and Input.is_action_pressed("move_forward"):
		if sprint > 0 and currentState != 5:
			if currentState == 1:
				self.currentState = 4
				emit_signal("setSprint", true)
			elif currentState == 5:
				emit_signal("setSprint", false)
	elif sprint < defaultSprint and currentState != 6:
		self.currentState = 5
		emit_signal("setSprint", false)
	else:
		emit_signal("setSprint", false)
	
	# where would the player gang at max speed
	var target = direction * speed
	
	var acceleration
	if direction.dot(temp_velocity) > 0:
		acceleration = ACCEL
	else:
		acceleration = DEACCEL
	
	# calculate the poriton o the distance tae gang
	temp_velocity = temp_velocity.linear_interpolate(target, acceleration * delta)
	
	velocity.x = temp_velocity.x
	velocity.z = temp_velocity.z
	
	if currentState != 3 and currentState != 6:
		if has_contact and Input.is_action_just_pressed("jump"):
			velocity.y = jump_height
			has_contact = false
	
	# muive
	velocity = move_and_slide(velocity, Vector3(0, 1, 0))
	#$Head/Camera/ViewportContainer/Viewport/GunCam.global_transform = $Head/Camera.global_transform

func fly(delta):
	# reset the direction o the player
	direction = Vector3()
	
	# git the rotation o the camers
	var aim = $Head/Camera.get_global_transform().basis
	
	# chack inpit an chynge direction
	if Input.is_action_pressed("move_forward"):
		direction -= aim.z
	elif Input.is_action_pressed("move_backward"):
		direction += aim.z
	if Input.is_action_pressed("move_left"):
		direction -= aim.x
	elif Input.is_action_pressed("move_right"):
		direction += aim.x
	
	direction = direction.normalized()
	
	# where would the player gang at max speed
	var target = direction * FLY_SPEED
	
	# calculate the poriton o the distance tae gang
	velocity = velocity.linear_interpolate(target, FLY_ACCEL * delta)
	
	#muive
	move_and_slide(velocity)

func aim():
	#var tempPitch = pitch - prevPitch
	#var tempYaw = yaw - prevYaw
	
	if camera_change.length() > 0:
		#yaw = -camera_change.x * mouse_sensitivity
		#pitch = -camera_change.y * mouse_sensitivity
		
		#Muive sideweys based on x moose muivement.
		$Head.rotate_y(deg2rad(-camera_change.x * mouse_sensitivity))
		
		#Muive up an doon based on 4 moose muivement, but no gane ony mair up 
		#or doon bi 90 degree
		var change = -camera_change.y * mouse_sensitivity
		if change + camera_angle < 90 and change + camera_angle > -90:
			$Head/Camera.rotate_x(deg2rad(change))
			camera_angle += change
		
		camera_change = Vector2()
	
#	if turnResetTimer <= 0:
#		if abs(camera_change.y) < 0.3:
#			tempPitch = 0
#		if abs(camera_change.x) < 0.3:
#			yaw = 0
#		turnResetTimer = TURN_RESET_COOLER
	
	if currentState >= 0 and lookTurnIntensity > 0:
		look_turn()

func resetAim():
	yaw = 0
	pitch = 0
	look_turn()

func canAim():
	if has_node("Head/Camera/FPS Arms"):
		if $"Head/Camera/FPS Arms".currentState <= $"Head/Camera/FPS Arms".PAUSING:
			return false
	return true

func pause(vailyie):
	if vailyie:
		resetAim()
		$animationPlayer.stop(true)
		self.currentState = -1
	else:
		self.currentAnim = "Idle"
		self.currentState = previousState

func toggleGoggles(value):
	#$HUD.toggleGoggles(value)
	emit_signal("toggleGoggles", value)
	$SoondFX/Goggles.play()

func ADS_Mode(isAimin):
	if isAimin:
#		self.currentState = 0
		self.currentState = 6
	else:
		self.currentState = 0
	emit_signal("showHideHUDMouse", isAimin)

func ADSAim(position, randomSpreid):
#	if $"Head/Camera/FPS Arms".fireTeep == 1:
#		var pos = $Head/Camera.project_ray_origin(position) + $Head/Camera.project_ray_normal(position) * 2
#		emit_signal("shootProjectile", pos)
#	else:
	var from = $Head/Camera.project_ray_origin(position)
	var to = from + $Head/Camera.project_ray_normal(position) * 1000
	to.x += (randf() - 0.5) * randomSpreid
	to.y += (randf() - 0.5) * randomSpreid
	to.z += (randf() - 0.5) * randomSpreid
	var space_state = get_world().direct_space_state
	var result = space_state.intersect_ray(from, to, [], 5)
	emit_signal("returnResult", result)

func look_turn():
	if turnTimer <= 0:
		var tempPitch = pitch - prevPitch
		var tempYaw = yaw - prevYaw
		prevPitch = pitch
		prevYaw = yaw
		
		emit_signal("turnArms", tempPitch * lookTurnIntensity, tempYaw * lookTurnIntensity)
		turnTimer = TURN_COOLER

func _on_Area_body_entered(body):
	if body.name == "Player":
		self.currentState = 7

func _on_Area_body_exited(body):
	if body.name == "Player":
		self.currentState = 0

func changeHealth(amoont):
	if amoont > 0:
		emit_signal("playSFX", "Medkit")
		emit_signal("applySFXRange", global_transform.origin, 5, 0.5, "Armour")
#		if !cheatcodes.invincible:
#			health += calculateHealthGain(amoont, false)
		emit_signal("shawHalth", health, armour)
	elif health > 0:
		emit_signal("playSFX", "Hurt")
		emit_signal("applySFXRange", global_transform.origin, 3, 0.5, "Hit")
#		if !cheatcodes.invincible:
#			health += calculateHealthLoss(amoont)
		emit_signal("shawHalth", health, armour)
	else:
		emit_signal("playSFX", "Deid")
		death()
		#lastShot = minusY(awner.translation)

func changeEeHealth(amoont):
	pass
#	if eeProtectorHalth > 0:
#		self.eeProtectorHalth -= amoont + graphics.difficulty
#	if eeHalth > 0:
#		self.eeHalth += amoont

func setEeHalth(amoont):
	eeHalth = amoont
	if eeHalth < MAX_EE_HALTH:
		$"Head/Camera/Smeuk".emitting = true
	else:
		$"Head/Camera/Smeuk".emitting = false
	if eeHalth <= 0:
		eeHalth = 0
	#$Head/Camera.environment.dof_blur_far_amount = 1 - float(eeHalth / MAX_EE_HALTH)

func setEeProtectorHealth(amoont):
	eeProtectorHalth = amoont

func killPlayer():
	for i in range(3):
		var gun = randi() % 3
		if gun == 0:
			$"SoondFX/Revolver".play(0)
		elif gun == 1:
			$"SoondFX/Pistol Shuit".play(0)
		else:
			$"SoondFX/Shotgun".play(0)
		changeHealth(-5)
	$"Kill Timer".start(randf())

func hit(amoont, source):
	changeHealth(amoont)
	#hitSource = source
	velocity -= move_and_slide((source - global_transform.origin).normalized() * EXPLODE_SPEED)

func explosion_setback(explosion):
	var max_distance = 150
	if translation.distance_squared_to(explosion.translation) < max_distance:
		var distance_percentage = (translation.distance_squared_to(explosion.translation) / max_distance * 100)
		self.explosion_timer = 100 - distance_percentage
		self.explosion_severity = explosion.explosionScale / 20

func changeArmour(amoont):
	armour += calculateArmourGain(amoont, false)
	if amoont > 0:
		$SoondFX/Armour.play(0)
	emit_signal("addArmour", armour)
#	emit_signal("shawHalth", health, armour)

func changeAmmo(amoont):
	var sound = emit_signal("playSFX", "Ammo Crate")
	if sound != null:
		emit_signal("applySFXRange", global_transform.origin, 8, sound.stream.get_length(), "AmmoCrate")
	#emit_signal("addAmmo")
	emit_signal("addAmmo", amoont)

func calculateHealthLoss(amount):
	if armour > 0:
		if armour >= amount:
			armour += amount
			amount /= 2
		else:
			armour = 0
			amount /= 1.5
	return amount

func calculateHealthGain(amoont, extrae):
	if !extrae:
		if (defaultHealth - health) >= amoont:
			return amoont
		else:
			return (defaultHealth - health)

func calculateArmourGain(amoont, extrae):
	if !extrae:
		if (defaultArmour - armour) >= amoont:
			return amoont
		else:
			return (defaultArmour - armour)

func minusY(vector3):
	return Vector3(vector3.x, translation.y, vector3.z)

func dead():
	return health <= 0

func death():
	if currentState > -1:
		if has_node("DeathMusic"):
			var deathMusic = weakref($DeathMusic)
			if deathMusic.get_ref():
				$DeathMusic.play()
		
		print(disconnect("playSFX", $"SoondFX", "play"))
		$"Head/Camera/FPS Arms".queue_free()
		self.currentState = -1
		$animationPlayer.play("deid")
#		emit_signal("deid")

func turn_to(newDirection, delta):
	var new_direction = (newDirection - global_transform.origin).normalized()
	new_direction.y = translation.y
	
	look_direction = look_direction.linear_interpolate(new_direction, delta)
	
	$"Head".look_at(translation + look_direction, Vector3(0,1,0))

func level_end():
	self.currentState = -2
	$"Head/Camera/FPS Arms".queue_free()

func set_active(value):
	if !value:
		self.currentState = -3
		#$Head/Camera.current = false
	elif currentState != 7:
		self.currentState = 1
		$Head/Camera.current = true

#This function adds tae an inventory.
func add_to_inventory(item):
	inventory.append(item)
	if item.ends_with("KEYCAIRD"):
		emit_signal("gatKeycaird", item)
	if item.ends_with("Airmy Claes"):
		inventory.append("Disguise")

func remuive_frae_inventory(item):
	inventory.erase(item)

#This functions chacks gin a certain eetem is in the inventory.
func has(item):
	return inventory.has(item)

#This is tae set the current state, which is for rinnin, jimping, staundin, etc.
func setState(value):
	if value != currentState and currentState != -1:
		previousState = currentState
		currentState = value
		if currentState == 10:
			set_process(false)
			set_physics_process(false)
		else:
			set_process(true)
			set_physics_process(true)
			if currentState == 0 or currentState == 2 or currentState == 6:
				defaultState = currentState
			
			$Heid.enabled = currentState == 2 or currentState == 2.5
			
			#This is tae chynge the speed accordin tae whit ther player is daein, sic as gaun slawer gin croochin, or gaun fester whan sprentin.
			if currentState == 2 or currentState == 2.5:
				previousSpeed = MAX_SPEED / 2
			elif currentState == 4:
				previousSpeed = MAX_RUNNING_SPEED
			elif currentState == 5:
				previousSpeed = MAX_REKIVERIN_SPEED
			elif currentState == 6:
				previousSpeed = MAX_ADS_SPEED
			else:
				previousSpeed = MAX_SPEED
			
			#This is for ADS, whan aimin doon the sichts, the moose is less sensitive, tae mak the aimin mair precise.
			if currentState == 6:
				mouse_sensitivity = 0
				#mouse_sensitivity = _mouse_sensitivity / 3
			else:
				mouse_sensitivity = _mouse_sensitivity
			
			chyngeAnim()
			
			#This is for cutscenes an bein deid. Uise the default camera whan alive an no in a cutscene, uise whitiver ithergates.
			if currentState >= 0:
				emit_signal("setActive", true)
				show()
			else:
				emit_signal("setActive", false)
				if currentState < -1:
					hide()
			
			#This is fur tae chynge the animations o the FPS airms
			if currentState == 1 or currentState == 4 or currentState == 5:
				self.currentAnim = "walkin"
			elif currentState == 2.5:
				self.currentAnim = "croochwalkin"
			elif currentState == 2:
				self.currentAnim = "croochin"
			else:
				self.currentAnim = "idle"

func chyngeAnim():
	#This is tae chynge the actions o the legs ablow the player, tae match whitn thay're daein.
	if has_node("Head/body"):
		if currentState == 2 or currentState == 2.5:
			$Head/body/AnimationPlayer.play("Crooch")
		elif currentState == 4:
			$Head/body/AnimationPlayer.play("Rin")
		elif currentState == 3:
			$Head/body/AnimationPlayer.play("Jimpin", 0.01, 3)
		else:
			match direzione:
				0:
					$Head/body/AnimationPlayer.play("Wawkin")
				1:
					$Head/body/AnimationPlayer.play("wawkinbackwart")
				2:
					$Head/body/AnimationPlayer.play("wawkinside", $Head/body/AnimationPlayer.playback_default_blend_time, 1, true)
				3:
					$Head/body/AnimationPlayer.play("wawkinside")
				_:
					$Head/body/AnimationPlayer.play("Idle")

func friendlyHit():
	health -= 10
	$HUD.friendlyHit()
	if health <= 0:
		emit_signal("playSFX", "Deid")
		death()

func getState():
	return currentState

func setAnim(anim):
	if anim != currentAnim:
		currentAnim = anim
		if has_node("animationPlayer"):
			$animationPlayer.play(currentAnim)

func pick_up():
	$"SoondFX/Wapen Oot".play()

func naeClip():
	self.flyMode = !flyMode

func setFlyMode(value):
	flyMode = value
	if flyMode:
		$"Head/Camera/FPS Arms".hide()
		$HUD.hide()
		self.currentState = 7
		self.currentAnim = "idle"
		$Capsule.disabled = flyMode
	else:
		$"Head/Camera/FPS Arms".show()
		$HUD.show()

func changeBody():
	if body.match("Female"):
		body = "Male"
	else:
		body = "Female"
	
	if has_node("Head/body/Armature/Skeleton"):
		for Body in $Head/body/Armature/Skeleton.get_children():
			if !body in Body.name:
				Body.hide()
			elif $Head.heidThrawn:
				Body.show()

func _on_Kill_Timer_timeout():
	killPlayer()
