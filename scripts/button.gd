extends MeshInstance

signal pressed

func _on_Area_body_entered(body):
	if body.is_in_group("Heid"):
		emit_signal("pressed")
		queue_free()
