extends Camera

export(int) var turnSpeed = 30
var look_direction = Vector3.UP

#A slawer version o "look_at()", it turns tae the target
#raither'n juist leukin straicht at it
func turn_to(newTarget, delta):
	newTarget.y -= 0.5
	var new_direction = (newTarget - global_transform.origin).normalized()
	
	look_direction = look_direction.linear_interpolate(new_direction, delta * turnSpeed)
	
	look_at(global_transform.origin + look_direction, Vector3.UP)
