extends RigidBody

var player

# Called when the node enters the scene tree for the first time.
func _ready():
	set_physics_process(false)

func fling(position, target):
	$Area/CollisionShape.disabled = true
	$CollisionShape.disabled = false
	look_at(position, Vector3(0, 1, 0))
	show()
	linear_velocity = Vector3(0, 0, 0)
	angular_velocity = Vector3(0, 0, 0)
	mode = MODE_RIGID
	apply_central_impulse((target - position) * 10)
	apply_central_impulse(Vector3(0, 1, 0))
	set_physics_process(true)
	$Timer.start(0.5)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	if Input.is_action_pressed("lookAtPlayer"):
		$Camera.turn_to(player.global_transform.origin, delta)

func unfling():
	$CollisionShape.disabled = true
	set_physics_process(false)
	sleeping = true
	$Camera.current = false

func _on_Area_body_entered(body):
	if body == player:
		body.get_node("Head").heid_back()
		unfling()

func _on_Timer_timeout():
	$Area/CollisionShape.disabled = false
