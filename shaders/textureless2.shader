shader_type spatial;
uniform vec4 albedo : hint_color;

void fragment() {
	vec2 base_uv = UV;
	ALBEDO = albedo.rgb;
}
