shader_type spatial;
uniform vec4 albedo : hint_color;
render_mode diffuse_lambert_wrap;

void fragment() {
	vec2 base_uv = UV;
	ALBEDO = albedo.rgb;
}
