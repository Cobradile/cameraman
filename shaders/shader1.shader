shader_type spatial;
uniform vec4 albedo : hint_color;
render_mode diffuse_lambert_wrap;
uniform sampler2D texture_albedo : hint_albedo;


vec4 sample_3P( vec2 st )//NOTE: GL_NEAREST must be used, and no mipmapping.
{
    //NOTE: probably a public uniform globabl in Godot ????
    vec2 TEXTURE_PIXEL_SIZE = vec2(textureSize(texture_albedo, 0));

    vec2 st_diagonal = st + vec2( 1.0/TEXTURE_PIXEL_SIZE.x, 1.0/TEXTURE_PIXEL_SIZE.y ) ; 
    vec4 a = texture( texture_albedo, st );
    vec4 c = texture( texture_albedo, st_diagonal );
    float x = fract( st.x*TEXTURE_PIXEL_SIZE.x ) ;
    float y = fract( st.y*TEXTURE_PIXEL_SIZE.y ) ;
    if( x > y ){
        vec4 b =  texture( texture_albedo, vec2( st_diagonal.x, st.y ) );
        return a*(1.0-x) + b*(x-y) + c*(y) ;
    }
    vec4 b =  texture( texture_albedo, vec2( st.x, st_diagonal.y ) );
    return a*(1.0-y) + b*(y-x) + c*(x) ; 
}

void fragment() {
	vec2 base_uv = UV;
	vec4 albedo_tex = sample_3P(base_uv);
	albedo_tex *= COLOR;
	ALBEDO = albedo.rgb * albedo_tex.rgb;
}
